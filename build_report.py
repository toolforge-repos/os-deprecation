#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Bryan Davis and contributors
# License: GPL-3.0-or-later
"""Wikimedia Cloud Services operating system deprecation report builder."""
import argparse
import logging
import os.path

import arrow
import jinja2

from keystone_browser import stats
import osd.report


class DirType(object):
    """
    Custom type for argparse.

    Based on https://stackoverflow.com/a/33181083/8171
    """

    def __call__(self, arg):
        """Check argument."""
        if arg == "-":
            # the special argument "-" means sys.std{in,out}
            raise argparse.ArgumentError(
                self, "standard input/output (-) not allowed as directory path"
            )
        e = os.path.exists(arg)
        if not e:
            raise argparse.ArgumentError(
                self, "path does not exist: '%s'" % arg
            )
        if not os.path.isdir(arg):
            raise argparse.ArgumentError(
                self, "path is not a directory: '%s'" % arg
            )
        return arg


def render_all(env, manifest, template_name):
    """Render the all projects report."""
    active = sum(
        1
        for proj, instances in manifest.items()
        for instance in instances
        if instance["status"] == "ACTIVE"
    )
    shutoff = sum(
        1
        for proj, instances in manifest.items()
        for instance in instances
        if instance["status"] == "SHUTOFF"
    )
    all_instances = stats.usage()["instances"]
    pct_active = round((active / all_instances) * 100)
    pct_shutoff = round((shutoff / all_instances) * 100)

    template = env.get_template(template_name)
    return template.render(
        manifest=manifest,
        active=active,
        pct_active=pct_active,
        shutoff=shutoff,
        pct_shutoff=pct_shutoff,
        all_instances=all_instances,
        updated=arrow.utcnow().format("YYYY-MM-DD HH:mm:ss"),
    )


def render_project(env, project, instances, template_name):
    """Render a project report."""
    template = env.get_template(template_name)
    return template.render(
        project=project,
        instances=instances,
        updated=arrow.utcnow().format("YYYY-MM-DD HH:mm:ss"),
    )


def main():
    """CLI interface."""
    parser = argparse.ArgumentParser(
        description="OS deprecation report generator"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        dest="loglevel",
        help="Increase logging verbosity",
    )
    parser.add_argument(
        "-e",
        "--exclude",
        action="append",
        help="Project to exclude from report (repeatable)",
    )
    parser.add_argument(
        "-t", "--template", default="report.html", help="Output template"
    )
    parser.add_argument(
        "-p",
        "--project-template",
        default="project.html",
        help="Project subreport output template",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=DirType(),
        required=True,
        help="Output directory",
    )
    parser.add_argument(
        "keyword", metavar="NAME", help="Keyword to look for in image name"
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=max(logging.DEBUG, logging.WARNING - (10 * args.loglevel)),
        format="%(asctime)s %(name)-12s %(levelname)-8s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
    )
    logging.captureWarnings(True)

    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(
            os.path.join(
                os.path.dirname(os.path.abspath(__file__)), "templates"
            )
        ),
        autoescape=jinja2.select_autoescape(["html", "xml"]),
    )

    manifest = osd.report.gather_data(args.keyword, args.exclude)

    with open(
        os.path.join(args.output, "{}.html".format(args.keyword)), "w"
    ) as report:
        report.write(render_all(env, manifest, args.template))

    project_dir = os.path.join(args.output, args.keyword)
    try:
        os.mkdir(project_dir)
    except FileExistsError:
        pass

    for project, instances in manifest.items():
        with open(
            os.path.join(project_dir, "{}.html".format(project)), "w"
        ) as report:
            report.write(
                render_project(env, project, instances, args.project_template)
            )


if __name__ == "__main__":
    main()
