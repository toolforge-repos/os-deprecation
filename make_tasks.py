#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Bryan Davis and contributors
# License: GPL-3.0-or-later
"""Wikimedia Cloud Services operating system deprecation task generator."""
import argparse
import logging
import urllib.parse

import yaml

from keystone_browser import keystone
from keystone_browser import ldap
import osd.phabricator
import osd.report


def main():
    """CLI interface."""
    parser = argparse.ArgumentParser(
        description="OS deprecation task generator"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        dest="loglevel",
        help="Increase logging verbosity",
    )
    parser.add_argument(
        "-c", "--config", default="tasks.yaml", help="Configuration file"
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=max(logging.DEBUG, logging.WARNING - (10 * args.loglevel)),
        format="%(asctime)s %(name)-12s %(levelname)-8s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
    )
    logging.captureWarnings(True)

    config = yaml.safe_load(open(args.config, "r"))
    phab = osd.phabricator.Client(
        url=config["phabricator"]["url"],
        username=config["phabricator"]["username"],
        token=config["phabricator"]["token"],
    )
    manifest = osd.report.gather_data(config["keyword"], config["exclude"])

    for project, instances in manifest.items():
        members = keystone.project_users_by_role(project)

        ldap_admins = ldap.get_users_by_uid(members["projectadmin"])
        admins = {admin["cn"]: admin for admin in ldap_admins}

        try:
            phab_admins = phab.user_ldapquery([u["cn"] for u in ldap_admins])
        except KeyError:
            phab_admins = []

        for admin in phab_admins:
            name = admin["ldap_username"]
            admins[name]["phabricator"] = "@{}".format(admin["userName"])
            admins[name]["phid"] = admin["phid"]

        fqdns = [
            "{name}.{tenant_id}.eqiad1.wikimedia.cloud".format(**instance)
            for instance in instances
        ]
        ctx = {
            "project": project,
            "instances": "\n".join(
                [
                    "[] [[{u}/{fqdn}|{fqdn}]]".format(
                        u="https://openstack-browser.toolforge.org/server",
                        fqdn=fqdn,
                    )
                    for fqdn in sorted(fqdns)
                ]
            ),
            "admins": "\n".join(
                [
                    "* [[{u}{ldap_enc}|wikitech:User:{ldap}]] ({phab})".format(
                        u="https://wikitech.wikimedia.org/wiki/User:",
                        ldap_enc=urllib.parse.quote(admin["cn"]),
                        ldap=admin["cn"],
                        phab=admin.get(
                            "phabricator",
                            "//Developer account not linked to Phabricator//",
                        ),
                    )
                    for key, admin in admins.items()
                ]
            ),
        }

        task = phab.new_task(
            title=config["task"]["title"].format(**ctx),
            description=config["task"]["description"].format(**ctx),
            tags=config["task"]["tags"],
            subscribers=[u["phid"] for u in phab_admins],
            column=config["task"]["column"],
            deadline=(
                int(config["task"]["deadline"].timestamp())
                if 'deadline' in config['task'] else None
            ),
        )
        print("{}: T{}".format(project, task["id"]))


if __name__ == "__main__":
    main()
