#!/usr/bin/env bash
# Generate a buster deprecation report
set -euf -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
$DIR/.venv/bin/python3 $DIR/build_report.py \
    --verbose \
    --exclude trove \
    --template buster.html \
    --project-template buster_project.html \
    --output $HOME/public_html \
    buster &&
cp $HOME/public_html/buster.html $HOME/public_html/index.html &&
find $HOME/public_html/buster -mtime +1 -type f -delete
