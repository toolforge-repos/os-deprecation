OS Deprecation Reports
======================

Generate reports on operating system deprecation projects in Wikimedia Cloud
VPS.

Install
-------
```
$ git clone https://gitlab.wikimedia.org/toolforge-repos/os-deprecation.git
$ cd tool-os-deprecation
$ git submodule update --init --recursive
$ webservice python3.9 shell
$ python3 -mvenv .venv
$ .venv/bin/pip install -U pip wheel
$ .venv/bin/pip install -U -r requirements.txt
$ exit # the webservice shell
$ toolforge-jobs load jobs.yaml
```

License
-------
[GNU GPLv3+](//www.gnu.org/copyleft/gpl.html "GNU GPLv3+")
