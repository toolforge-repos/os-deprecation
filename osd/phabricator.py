# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Bryan Davis and contributors
# License: GPL-3.0-or-later
"""Phabricator client."""

import json
import logging

import requests


logger = logging.getLogger(__name__)


class APIError(Exception):
    """Phabricator API error."""

    def __init__(self, message, code, result):
        """Create."""
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        """As string."""
        return "{0} ({1})".format(self.message, self.code)


class Client(object):
    """Phabricator client."""

    def __init__(self, url, username, token):
        """Create.

        Args:
            url (string): Phabricator base URL
            username (string): Username
            token (string): Phabricator API token for username

        """
        self.url = url
        self.username = username
        self.session = {"token": token}

    def post(self, path, data):
        """HTTP POST.

        Args:
            path (string): API path
            data (dict): API request payload

        """
        data["__conduit__"] = self.session
        r = requests.post(
            "{0}/api/{1}".format(self.url, path),
            data={"params": json.dumps(data), "output": "json"},
        )
        resp = r.json()
        logger.debug("%s result: %s", path, resp)
        if resp["error_code"] is not None:
            raise APIError(
                resp["error_info"],
                resp["error_code"],
                resp.get("result", None),
            )
        return resp["result"]

    def user_ldapquery(self, names):
        """Lookup Phabricator user data associated with LDAP cn values."""
        names = list(filter(None, names))
        try:
            r = self.post(
                "user.ldapquery",
                {"ldapnames": names, "offset": 0, "limit": len(names)},
            )
        except APIError as e:
            if (
                e.code == "ERR-INVALID-PARAMETER"
                and "Unknown or missing ldap names" in str(e)
            ):
                logger.exception("user.ldapquery lookup failure")
                if e.result is None:
                    raise KeyError(
                        "Users not found for {0}".format(", ".join(names))
                    )
                else:
                    # Return the partial result
                    r = e.result
            else:
                raise e
        else:
            return r

    def phid_lookup(self, name):
        """Lookup an object by PHID."""
        r = self.post("phid.lookup", {"names": [name]})
        if name in r:
            return r[name]
        raise KeyError("PhID {0} not found".format(name))

    def new_task(
        self,
        title,
        description,
        tags,
        priority="normal",
        parents=None,
        subscribers=None,
        column=None,
        deadline=None,
    ):
        """Create a new maniphest task.

        Args:
            title (string): Title of task.
            description (string): Description of task, formatted as remarkup
            tags (list(string)): List of tags to add to task
            priority (string): Priority of task
            parents (list(string)): List of parent tasks
            subscribers (list(string)): List of subscriber PHIDs
            column (string): Column PHID
            deadline (int): Unix timestamp of task due date

        """
        transactions = [
            {"type": "title", "value": title},
            {"type": "description", "value": description},
            {
                "type": "projects.set",
                "value": [self.phid_lookup(tag)["phid"] for tag in tags],
            },
            {"type": "priority", "value": priority},
        ]
        if parents:
            transactions.append(
                {
                    "type": "parents.set",
                    "value": [self.phid_lookup(t)["phid"] for t in parents],
                }
            )
        if subscribers:
            transactions.append(
                {"type": "subscribers.set", "value": subscribers}
            )
        if column:
            transactions.append({"type": "column", "value": [column]})
        if deadline:
            transactions.append(
                {"type": "custom.deadline.due", "value": deadline}
            )

        r = self.post("maniphest.edit", {"transactions": transactions})
        return self.task(r["object"]["phid"])

    def task(self, phid):
        """Lookup details of a Maniphest task."""
        r = self.post("maniphest.search", {"constraints": {"phids": [phid]}})
        if r["data"]:
            return r["data"][0]
        raise Exception("No task found for phid %s" % phid)
