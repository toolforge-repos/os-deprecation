# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 Bryan Davis and contributors
# License: GPL-3.0-or-later
"""Gather report data."""

from keystone_browser import glance
from keystone_browser import keystone
from keystone_browser import nova


def gather_data(keyword, exclude_projects=None):
    """Create a dict of lists of questionable instances indexed by project."""
    images = {uuid: data["name"] for uuid, data in glance.images().items()}
    manifest = {}
    if exclude_projects is None:
        exclude_projects = []

    for project in keystone.all_projects():
        if project in exclude_projects:
            # Ignore everything about excluded projects
            continue
        deprecated = []
        servers = nova.project_servers(project)
        for server in servers:
            image_id = server["image"]["id"]
            image_name = images.get(
                image_id, "unknown image (possibly {})".format(keyword)
            )
            if keyword in image_name:
                deprecated.append(
                    {
                        "name": server["name"],
                        "tenant_id": server["tenant_id"],
                        "image": image_name,
                        "status": server["status"],
                    }
                )
        if deprecated:
            manifest[project] = deprecated
    return manifest
